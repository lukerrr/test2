// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TESTBLANK2_TestBlank2GameModeBase_generated_h
#error "TestBlank2GameModeBase.generated.h already included, missing '#pragma once' in TestBlank2GameModeBase.h"
#endif
#define TESTBLANK2_TestBlank2GameModeBase_generated_h

#define TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_RPC_WRAPPERS
#define TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATestBlank2GameModeBase(); \
	friend TESTBLANK2_API class UClass* Z_Construct_UClass_ATestBlank2GameModeBase(); \
public: \
	DECLARE_CLASS(ATestBlank2GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/TestBlank2"), NO_API) \
	DECLARE_SERIALIZER(ATestBlank2GameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesATestBlank2GameModeBase(); \
	friend TESTBLANK2_API class UClass* Z_Construct_UClass_ATestBlank2GameModeBase(); \
public: \
	DECLARE_CLASS(ATestBlank2GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/TestBlank2"), NO_API) \
	DECLARE_SERIALIZER(ATestBlank2GameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATestBlank2GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATestBlank2GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATestBlank2GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATestBlank2GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATestBlank2GameModeBase(ATestBlank2GameModeBase&&); \
	NO_API ATestBlank2GameModeBase(const ATestBlank2GameModeBase&); \
public:


#define TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATestBlank2GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATestBlank2GameModeBase(ATestBlank2GameModeBase&&); \
	NO_API ATestBlank2GameModeBase(const ATestBlank2GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATestBlank2GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATestBlank2GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATestBlank2GameModeBase)


#define TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_12_PROLOG
#define TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_RPC_WRAPPERS \
	TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_INCLASS \
	TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestBlank2_Source_TestBlank2_TestBlank2GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
